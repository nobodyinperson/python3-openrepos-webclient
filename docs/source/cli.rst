Command-Line Interface
======================

:mod:`openrepos` comes with a :doc:`cli`.

Invocation
++++++++++

:mod:`openrepos`'s :doc:`cli` is invoked from the command-line by launching the
``openrepos`` command:

.. code-block:: sh

    openrepos
    # Usage: openrepos [OPTIONS] COMMAND [ARGS]...
    #
    #  OpenRepos.net web client
    # ...

If running the above fails with something sounding like ``command not found:
openrepos``, you may either always run ``python3 -m openrepos`` instead of a
plain ``openrepos`` or add the directory ``~/.local/bin`` to your ``PATH``
environment variable by appending the following line to your shell's
configuration (probabily ``.bashrc`` in your home directory):

.. code-block:: sh

   export PATH="$HOME/.local/bin:$PATH"

The :doc:`cli` is based on `Click`_ and requires a subcommand to be specified.
The available subcommands will be explained throughout this page.

Help
----

A help page is available for both the ``openrepos`` command and each
subcommand. To access it, pass the ``--help`` (or ``-h``) option to it:

.. code-block:: sh

    openrepos --help
    # Usage: openrepos [OPTIONS] COMMAND [ARGS]...
    #
    #  OpenRepos.net web client
    # ...


.. _cli logging:

Logging
-------

If something is not working as expected or the :doc:`cli` seems to ”hang”, you
might want to check some more verbose logging output to see what's going on.
You can increase the verbosity of the :doc:`cli` by passing the ``--verbose``
(or ``-v``) option one or multiple times to the ``openrepos`` command:

.. code-block:: sh

   openrepos -v ...
   # a little more output
   # ...
   openrepos -vvvv ...
   # VERY much logging output
   # ...
   OPENREPOS_VERBOSITY=4 openrepos
   # same as the above four -v

Analogously, if you'd like less verbosity, specify the ``--quiet`` (or ``-q``
or the ``OPENREPOS_QUIETNESS`` environment variable)
options one or more times.

.. hint::

   Keep in mind that you have to specify the logging options **to the**
   ``openrepos`` **command, BEFORE any subcommand**!

.. _Click: https://click.palletsprojects.com


