Welcome to the OpenRepos Python WebClient documentation!
========================================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   cli
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
